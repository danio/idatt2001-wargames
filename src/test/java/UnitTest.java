import Main.Terrain;
import Unit.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConcreteDefaultUnit extends Unit {

    public ConcreteDefaultUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }
    public ConcreteDefaultUnit(ConcreteDefaultUnit unit) {
        super(unit);
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        return 0;
    }

    public int getAttackBonus() {
        return 0;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        return 0;
    }

    @Override
    public ConcreteDefaultUnit clone() {
        return new ConcreteDefaultUnit(this);
    }
}

class UnitTest {
    @Test
    void attackOther() {
        var attacker = new ConcreteDefaultUnit("Rylai, the Crystal Maiden", 560, 47, 1);
        var defender = new ConcreteDefaultUnit("Lina, the Slayer", 600, 55, 3);

        attacker.attack(defender, Terrain.PLAINS);

        /*
            600 - 47 (attack) + 3 (defense) == 556
         */
        assertEquals(556, defender.getHealth());
    }

    /**
     * Should this be allowed??
     */
    @Test
    void attackSelf() {
        var attacker = new ConcreteDefaultUnit("Lion, the Demon Witch", 560, 50, 2);
        var defender = attacker;

        attacker.attack(defender, Terrain.PLAINS);

        // 560 - 50 (attack) + 2 (defense) == 512
        assertEquals(512, defender.getHealth());
    }

    @Test
    void attackUnderflow() {
        var attacker = new ConcreteDefaultUnit("God", Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);

        // Does absolutely nothing and then dies
        var defender = new ConcreteDefaultUnit("Glenn, the Glonk", 1, 0, Integer.MIN_VALUE);

        assertThrowsExactly(ArithmeticException.class, () -> attacker.attack(defender, Terrain.PLAINS));
    }

    @Test
    void receiveAttack() {
        var defender = new ConcreteDefaultUnit("Rhasta, the Shadow Shaman", 660, 74, 4);

        defender.receiveAttack(100, Terrain.PLAINS);
        // 660 - 100 + 4
        assertEquals(564, defender.getHealth());
    }
}