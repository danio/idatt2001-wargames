import Main.Terrain;
import Unit.InfantryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    @Test
    void attack() {
        var attacker = new InfantryUnit("Zharvakko, the Witch Doctor", 560);
        var defender = new InfantryUnit("Rigwarl, the Bristleback", 640);

        attacker.attack(defender, Terrain.FOREST);

        // 640 - 15 - 2 (attack) + 10 + 1(defense) == 634
        assertEquals(634, defender.getHealth());
    }
}