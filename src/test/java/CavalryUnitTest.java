import Main.Terrain;
import Unit.CavalryUnit;
import Unit.InfantryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {

    // I normally wouldn't test all this, it's not that complicated and the tests for Unit.Unit is pretty much good enough.
    @Test
    void getAttackBonus() {
        var attacker = new CavalryUnit("Shendelzare, the Vengeful Spirit", 580);
        var defender = new InfantryUnit("Aggron Stonebreak, the Ogre Magi", 660);

        assertEquals(7, attacker.getAttackBonus(Terrain.PLAINS));
        assertEquals(5, attacker.getAttackBonus(Terrain.FOREST));

        attacker.attack(defender, Terrain.PLAINS);
        assertEquals(2, attacker.getAttackBonus(Terrain.PLAINS));
    }
}