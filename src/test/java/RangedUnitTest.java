import Main.Terrain;
import Unit.RangedUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {

    @Test
    void getResistBonus() {
        var defender = new RangedUnit("Demnok Lannik, the Warlock", 680);

        var bonus = defender.getResistBonus(Terrain.PLAINS);
        assertEquals(6, bonus);

        defender.receiveAttack(100, Terrain.PLAINS);
        bonus = defender.getResistBonus(Terrain.PLAINS);
        assertEquals(4, bonus);

        defender.receiveAttack(100, Terrain.PLAINS);
        bonus = defender.getResistBonus(Terrain.PLAINS);
        assertEquals(2, bonus);

        defender.receiveAttack(100, Terrain.PLAINS);
        bonus = defender.getResistBonus(Terrain.PLAINS);
        assertEquals(2, bonus);

        defender.receiveAttack(100, Terrain.PLAINS);
        bonus = defender.getResistBonus(Terrain.PLAINS);
        assertNotEquals(0, bonus);
    }

    @Test
    void receiveAttack() {
        var defender = new RangedUnit("Nortrom, the Silencer", 580);

        defender.receiveAttack(100, Terrain.PLAINS);
        // 580 - 100 + 6 + 8
        assertEquals(494, defender.getHealth());
    }
}