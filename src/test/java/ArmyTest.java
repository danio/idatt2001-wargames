import Main.Army;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Unit.*;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    Army setup() {
        var units = new ArrayList<Unit>();
        return new Army("Radiant", units, new Random(123));
    }

    @Test
    void addAll() {
        var army = setup();

        var list = new ArrayList<Unit>(4);

        list.addAll(UnitFactory.getUnits("InfantryUnit", 3, "Meele Creep", 550));
        list.add(new RangedUnit("Ranged Creep", 300));

        army.addAll(list);

        assertTrue(army.getAllUnits().equals(list));

        list.add(new CavalryUnit("Siege Creep", 935));
        assertNotEquals(true, army.getAllUnits().equals(list));
    }

    // TODO: test deep copying once that's done
    @Test
    void getAllUnits() {
        var army = setup();
        army.addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));
        var units = army.getAllUnits();
        var cheater = new CavalryUnit("Cheater Skeeter", 9999, 9999, 9999);
        assertThrowsExactly(UnsupportedOperationException.class, () -> units.add(cheater));
    }

    // How do you even test randomness, test it once with a set seed,
    // say that's the truth and hope it doesn't change when it shouldn't?
    // Maybe you should make a custom rng thing that isn't actually rng at all.
    // TODO: Make this not awful?
    @Test
    void getRandom() {
        var army = setup();

        army.addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));

        var unit1 = army.getRandom();
        var unit2 = army.getRandom();

        // This doesn't introduce randomness in the tests because of the set seed in setup()
        // It also doesn't test anything really.
        assertNotEquals(unit1, unit2);
    }

    @Test
    void getUnitsFilter() {
        var army = setup();
        army.addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));
        var waldo = UnitFactory.getUnit("InfantryUnit", "Waldo", 100);
        army.add(waldo);
        army.addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));
        var woof = UnitFactory.getUnit("InfantryUnit", "Woof", 30);
        army.add(woof);

        var result = army.getUnitsFilter(u -> u.getName().equals("Waldo")); // Where's waldo?

        assertTrue(result.contains(waldo));
        assertFalse(result.contains(woof));
    }

    @Test
    void getUnitsOfClass() {
        var army = setup();

        army.addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));
        var waldo = UnitFactory.getUnit("InfantryUnit", "Waldo", 100);
        army.add(waldo);
        army.addAll(UnitFactory.getUnits("RangedUnit", 5, "Ranged Creep", 300));
        var woof = UnitFactory.getUnit("InfantryUnit", "Woof", 30);
        army.add(woof);

        var result = army.getUnitsOfClass(RangedUnit.class);

        // This is kind of guaranteed already but ok:

        //Tests whether or not it is a List<RangedUnit>
        assertTrue(Arrays.asList(result.getClass().getInterfaces()).contains(List.class));
        assertEquals(RangedUnit.class, result.stream().findFirst().get().getClass());

        // I literally cannot add tests that would do something illegal as it won't compile
        // It also uses the previous function that is tested so the other edge cases are covered.
    }
}