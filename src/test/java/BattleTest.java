import Main.Army;
import Main.Terrain;
import Unit.CavalryUnit;
import Unit.InfantryUnit;
import Unit.RangedUnit;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BattleTest {

    @Test
    void simulate() {
        var rng = new Random(123);
        var radiant = new Army("Radiant", rng);
        var dire = new Army("Dire", rng);

        for (int i = 0; i < 30; i++) {
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new RangedUnit("Ranged Creep", 300));

            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new RangedUnit("Ranged Creep", 300));
        }
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));


        for (int i = 30; i < 61; i++) {
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new RangedUnit("Ranged Creep", 300));

            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new RangedUnit("Ranged Creep", 300));
        }

        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));

        for (int i = 70; i < 81; i++) {
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new RangedUnit("Ranged Creep", 300));

            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new RangedUnit("Ranged Creep", 300));
        }

        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));

        for (int i = 80; i < 91; i++) {
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new RangedUnit("Ranged Creep", 300));
            radiant.add(new RangedUnit("Ranged Creep", 300));

            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new RangedUnit("Ranged Creep", 300));
            dire.add(new RangedUnit("Ranged Creep", 300));
        }

        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        radiant.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));
        dire.add(new CavalryUnit("Siege Creep", 935));

        var battle = new Main.Battle(radiant, dire, Terrain.PLAINS, rng);

        // This only tests that it didn't change in the future without doing it on purpose.
        // See it more as a canary than a spec-test
        assertEquals(radiant, battle.simulate());
    }

    @Test
    void noDeadUnits() {
        var rng = new Random(123);
        var radiant = new Army("Radiant", rng);
        var dire = new Army("Dire", rng);

        for (int i = 0; i < 30; i++) {
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new InfantryUnit("Meele Creep", 550));
            radiant.add(new RangedUnit("Ranged Creep", 300));

            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new InfantryUnit("Meele Creep", 550));
            dire.add(new RangedUnit("Ranged Creep", 300));
        }

        var battle = new Main.Battle(radiant, dire, Terrain.PLAINS, rng);

        var victor = battle.simulate();

        for (var unit : victor.getAllUnits()) {
            assertTrue(unit.getHealth() > 0);
        }
    }
}