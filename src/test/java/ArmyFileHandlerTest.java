import Main.Army;
import Main.ArmyFileHandler;
import Unit.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class ArmyFileHandlerTest {

    Army setup() {
        var units = new ArrayList<Unit>();
        return new Army("Radiant", units, new Random(123));
    }

    @Test
    void toCsv() {
        var army = setup();
        var list = new ArrayList<Unit>();

        IntStream.range(0, 5).forEach(i -> {
            var unit = new InfantryUnit("Meele Creep", 550);
            list.add(unit);
        });
        var waldo = new InfantryUnit("Waldo", 100);
        list.add(waldo);
        IntStream.range(0, 5).forEach(i -> {
            var unit = new RangedUnit("Ranged Creep", 300);
            list.add(unit);
        });
        var woof = new InfantryUnit("Woof", 30);
        list.add(woof);

        army.addAll(list);

        var result = ArmyFileHandler.toCsv(army);

        StringBuilder wanted = new StringBuilder();
        wanted.append("Radiant\n");
        wanted.append("InfantryUnit,Meele Creep,550,15,10\n");
        wanted.append("InfantryUnit,Meele Creep,550,15,10\n");
        wanted.append("InfantryUnit,Meele Creep,550,15,10\n");
        wanted.append("InfantryUnit,Meele Creep,550,15,10\n");
        wanted.append("InfantryUnit,Meele Creep,550,15,10\n");
        wanted.append("InfantryUnit,Waldo,100,15,10\n");
        wanted.append("RangedUnit,Ranged Creep,300,15,8\n");
        wanted.append("RangedUnit,Ranged Creep,300,15,8\n");
        wanted.append("RangedUnit,Ranged Creep,300,15,8\n");
        wanted.append("RangedUnit,Ranged Creep,300,15,8\n");
        wanted.append("RangedUnit,Ranged Creep,300,15,8\n");
        wanted.append("InfantryUnit,Woof,30,15,10\n");

        assertEquals(wanted.toString(), result);

    }

    @Test
    void fromCsv() {
        StringBuilder csv = new StringBuilder();
        csv.append("Radiant\n");
        csv.append("InfantryUnit,Meele Creep,550,15,10\n");
        csv.append("InfantryUnit,Meele Creep,550,15,10\n");
        csv.append("InfantryUnit,Meele Creep,550,15,10\n");
        csv.append("InfantryUnit,Meele Creep,550,15,10\n");
        csv.append("InfantryUnit,Meele Creep,550,15,10\n");
        csv.append("InfantryUnit,Waldo,100,15,10\n");
        csv.append("RangedUnit,Ranged Creep,300,15,8\n");
        csv.append("RangedUnit,Ranged Creep,300,15,8\n");
        csv.append("RangedUnit,Ranged Creep,300,15,8\n");
        csv.append("RangedUnit,Ranged Creep,300,15,8\n");
        csv.append("RangedUnit,Ranged Creep,300,15,8\n");
        csv.append("InfantryUnit,Woof,30,15,10\n");

        var wanted = setup();
        var list = new ArrayList<Unit>();

        IntStream.range(0, 5).forEach(i -> {
            var unit = new InfantryUnit("Meele Creep", 550, 15, 10);
            list.add(unit);
        });
        var waldo = new InfantryUnit("Waldo", 100, 15, 10);
        list.add(waldo);
        IntStream.range(0, 5).forEach(i -> {
            var unit = new RangedUnit("Ranged Creep", 300, 15, 8);
            list.add(unit);
        });
        var woof = new InfantryUnit("Woof", 30, 15, 10);
        list.add(woof);
        wanted.addAll(list);

        var result = ArmyFileHandler.fromCsv(csv.toString());

        // Weak assertion due to similar being vague
        assertTrue(wanted.similar(result));
    }

}