package Unit;

/**
 * The commander unit is the strongest unit and is a cavalry unit
 */
public class CommanderUnit extends CavalryUnit {
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Make a new commander unit, attack defaults to 25, armor defaults to 15
     *
     * @param name   Name of unit
     * @param health Initial health of unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    public CommanderUnit(CommanderUnit unit) {
        super(unit);
    }

    @Override
    public CommanderUnit clone() {
        return new CommanderUnit(this);
    }
}
