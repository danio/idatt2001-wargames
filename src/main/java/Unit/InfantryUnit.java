package Unit;

import Main.Terrain;

/**
 * Infantry units are the most basic units
 */
public class InfantryUnit extends Unit {
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Make a new Infantry unit, attack defaults to 15, armor defaults to 10
     *
     * @param name   Name of unit
     * @param health Initial health of unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    public InfantryUnit(InfantryUnit unit) {
        super(unit);
    }

    @Override
    public InfantryUnit clone() {
        return new InfantryUnit(this);
    }

    /**
     * Bonus to attack but only in close-combat
     * {@inheritDoc}
     *
     * @return bonus based on terrain
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        if (terrain == Terrain.FOREST) {
            return 2;
        } else return 0;
    }

    /**
     * Small bonus to defense while in forest
     *
     * {@inheritDoc}
     *
     * @return defense bonus
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        if (terrain == Terrain.FOREST) {
            return 1;
        } else return 0;
    }
}
