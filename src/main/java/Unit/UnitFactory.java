package Unit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Class for allocating new Unit objects
 */
public class UnitFactory {
    /**
     * Create unit
     * @param unitType string representing a unit, {@see #values}
     * @param name name of the created unit
     * @param health health of the created unit
     * @param attack attack damage of the created unit
     * @param armor armor value of the created unit
     * @return a new object representing the unit
     */
    public static Unit getUnit(String unitType, String name, int health, int attack, int armor) {
        if (unitType == null) {
            throw new NullPointerException("UnitType should not be null");
        }
        if (health < 0 ){
            throw new IllegalArgumentException("Health shouldn't be negative for new units");
        }

        switch (unitType) {
            case "InfantryUnit":
                return new InfantryUnit(name, health, attack, armor);
            case "CavalryUnit":
                return new CavalryUnit(name, health, attack, armor);
            case "RangedUnit":
                return new RangedUnit(name, health, attack, armor);
            case "CommanderUnit":
                return new CommanderUnit(name, health, attack, armor);
        }

        throw new IllegalArgumentException("Unit type was not recognized");
    }
    public static Unit getUnit(String unitType, String name, int health) {
        if (unitType == null) {
            throw new NullPointerException("UnitType should not be null");
        }
        if (health < 0 ){
            throw new IllegalArgumentException("Health shouldn't be negative for new units");
        }

        switch (unitType) {
            case "InfantryUnit":
                return new InfantryUnit(name, health);
            case "CavalryUnit":
                return new CavalryUnit(name, health);
            case "RangedUnit":
                return new RangedUnit(name, health);
            case "CommanderUnit":
                return new CommanderUnit(name, health);
        }

        throw new IllegalArgumentException("Unit type was not recognized");
    }

    public static <C extends Collection<Unit>> C getUnits(String unitType, int n, String name, int health, int attack, int armor, Supplier<C> collectionFactory) {
        return IntStream.range(0, n).mapToObj(i -> UnitFactory.getUnit(unitType, name, health, attack, armor)).collect(Collectors.toCollection(collectionFactory));
    }

    public static <C extends Collection<Unit>> C getUnits(String unitType, int n, String name, int health, Supplier<C> collectionFactory) {
        return IntStream.range(0, n).mapToObj(i -> UnitFactory.getUnit(unitType, name, health)).collect(Collectors.toCollection(collectionFactory));
    }

    public static Collection<Unit> getUnits(String unitType, int n, String name, int health, int attack, int armor) {
        return IntStream.range(0, n).mapToObj(i -> UnitFactory.getUnit(unitType, name, health, attack, armor)).collect(Collectors.toSet());
    }

    public static Collection<Unit> getUnits(String unitType, int n, String name, int health) {
        return IntStream.range(0, n).mapToObj(i -> UnitFactory.getUnit(unitType, name, health)).collect(Collectors.toSet());
    }

    /**
     * Similar to an enum's values funciton, return a "canonical" list of all Unit types
     * @return collection of available units
     */
    public static Collection<String> values() {
        var list = new ArrayList<String>();
        list.add("InfantryUnit");
        list.add("CavalryUnit");
        list.add("RangedUnit");
        list.add("CommanderUnit");

        return list;
    }
}
