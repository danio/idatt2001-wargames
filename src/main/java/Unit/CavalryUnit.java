package Unit;

import Main.Terrain;

/**
 * Cavalry units get a bonus the first time they charge in with an attack
 */
public class CavalryUnit extends Unit {
    private boolean hasAttacked = false;

    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Make a new Cavalry unit, attack defaults to 20, armor defaults to 12
     *
     * @param name   Name of unit
     * @param health Initial health of unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    public CavalryUnit(CavalryUnit cavalryUnit) {
        super(cavalryUnit);
        this.hasAttacked = cavalryUnit.getHasAttacked();
    }

    @Override
    public CavalryUnit clone() {
        return new CavalryUnit(this);
    }

    /**
     * Gets whether the unit has ever attacked
     *
     * @return Whether the unit has attacked in its life
     */
    private boolean getHasAttacked() {
        return hasAttacked;
    }

    /**
     * @param opponent Another unit being attacked. The one which will take damage.
     */
    @Override
    public void attack(Unit opponent, Terrain terrain) {
        super.attack(opponent, terrain);
        hasAttacked = true;
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        if (!getHasAttacked()) {
            return 5 + ((terrain == Terrain.PLAINS) ? 2 : 0);
        } else {
            return 2;
        }
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        if (terrain == Terrain.FOREST) return 0;
        return 1;
    }
}
