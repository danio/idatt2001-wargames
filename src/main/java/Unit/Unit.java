package Unit;

import Main.Terrain;

/**
 * Abstract class all units are derived from
 */
public abstract class Unit {
    private final String name;
    private final int attack;
    private final int armor;
    private int health;

    /**
     * Constructor for a Unit.Unit
     *
     * @param name   Name of this unit
     * @param health The units starting health
     * @param attack The units base attack damage
     * @param armor  The unit's base armor
     * @throws NullPointerException     if name is null
     * @throws IllegalArgumentException if health is less than 0
     */
    public Unit(String name, int health, int attack, int armor) {
        if (name == null) {
            throw new NullPointerException("name of unit can't be null");
        }
        if (health < 0) {
            throw new IllegalArgumentException("health cannot be a negative value");
        }

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    public Unit(Unit unit) {
        this(unit.getName(), unit.getHealth(), unit.getAttack(), unit.getArmor());
    }

    /**
     * Deep copy the unit
     * This should have been Clonable Interface probably
     * @return deep copy of unit
     */
    public abstract Unit clone();

    /**
     * Attack another unit.
     * A unit calling this will do one attack on another unit. Dealing damage with the following formula:
     * (attack + attack bonus) - (opponent armor + opponent resist bonus)
     * If an attack is weak compared to the defense the opponent can heal.
     *
     * @param opponent Another unit being attacked. The one which will take damage.
     * @throws NullPointerException if opponent is null
     * @throws ArithmeticException  if the values involved would overflow or underflow the opponents health
     * @see #receiveAttack(int, Terrain)
     */
    public void attack(Unit opponent, Terrain terrain) {
        if (opponent == null) throw new NullPointerException("opponent can not be null");
        var damage = Math.addExact(getAttack(), getAttackBonus(terrain));

        opponent.receiveAttack(damage, terrain);
    }

    /**
     * Can mutate other parts of the unit, can customize how damage is dealt to the unit
     * <p>
     * The default implementation uses the formula newHealth = oldHealth - damage + (armor + resistBonus)
     * <p>
     * I toyed with the idea of adding Unit.Unit attacker to the function here, but decided the complications of opening up
     * for recursive attacks would lead to more problems that what it was worth. Adding things like damage reflection
     * will need to be done some other way
     *
     * @param damage how much damage is the enemy attempting to deal
     * @throws ArithmeticException if the values involved would underflow or overflow health
     * @see #getResistBonus(Terrain)
     */
    public void receiveAttack(int damage, Terrain terrain) {
        var defense = Math.addExact(getArmor(), getResistBonus(terrain));
        var newHealth = Math.subtractExact(getHealth(), Math.subtractExact(damage, defense));
        setHealth(newHealth);
    }

    /**
     * @return name of unit
     */
    public String getName() {
        return name;
    }

    /**
     * @return type of unit
     */
    public String getType() {
        return this.getClass().getSimpleName();
    }

    /**
     * Get current health of unit
     *
     * @return Current health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Sets the health of the unit
     * Could be private since it's only used in #receiveAttack but task specifies it's public
     *
     * @param health The new health of the unit
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * @return The attack value for the unit
     */
    public int getAttack() {
        return attack;
    }

    /**
     * @return The armor value for the unit
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Get a textual representation of the unit
     *
     * @return String in form of &lt;name&gt;, &lt;health&gt; HP &lt;attack&gt; ATK &lt;armor&gt; DEF
     */
    @Override
    public String toString() {
        return name + ", " + health + " HP " + attack + " ATK" + armor + " DEF";
    }

    /**
     * Function is used by the default {@link #attack(Unit, Terrain)} implementation
     *
     * @return some bonus to apply while attacking
     */
    // Realistically these would be private due to the receiveDamage function, but since task says they should be public they are public
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * Function is used by the default {@link #receiveAttack(int, Terrain)} implementation
     *
     * @return some bonus to apply while defending from an attack
     */
    public abstract int getResistBonus(Terrain terrain);
}
