package Unit;

import Main.Terrain;

/**
 * Ranged units get a bonus while they are attacking or defending via a distance
 */
public class RangedUnit extends Unit {

    private int attackedCounter = 0;

    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Make a new Unit.RangedUnit, attack defaults to 15, armor defaults to 8
     *
     * @param name   Name of unit
     * @param health Initial health of unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    public RangedUnit(RangedUnit unit) {
        super(unit);
        this.attackedCounter = unit.getAttackedCounter();
    }

    @Override
    public RangedUnit clone() {
        return new RangedUnit(this);
    }

    /**
     * @return how many times unit has been attacked
     */
    private int getAttackedCounter() {
        return attackedCounter;
    }

    /**
     * Increments the attackCounter up until 2
     */
    private void incrementAttackedCounter() {
        attackedCounter++;
        if (attackedCounter > 2) {
            attackedCounter = 2;
        }
    }

    /**
     * Ranged units get attack bonus from being ranged
     *
     * @return 3
     * @see #attack(Unit)
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        switch (terrain) {
            case HILL:
                return 5;
            case FOREST:
                return 3;
            case PLAINS:
                return 0;
        }
        return 0;
    }

    /**
     * Ranged units get their defense from being a distance away from the enemy
     * As we are attacked we assume the enemy is approaching, and reduce the defense bonus.
     *
     * @return 6 the first time, 4, the second time, 2 the rest of the times
     * @see #receiveAttack(int)
     */
    @Override
    public int getResistBonus(Terrain terrain) {

        // I don't like switch statements as they have too many "clever" gotcha features
        // Manually implementing jump tables is not something that should be considered normal
        // I therefore reach for an if-ladder in languages that don't have modern match statements.
        if (getAttackedCounter() == 0) {
            return 6;
        } else if (getAttackedCounter() == 1) {
            return 4;
        } else if (getAttackedCounter() >= 2) {
            return 2;
        }
        return 0; // Make the java compiler happy
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation also increments the attacked counter
     *
     * @see #getResistBonus()
     */
    @Override
    public void receiveAttack(int damage, Terrain terrain) {
        super.receiveAttack(damage, terrain);
        incrementAttackedCounter();
    }
}
