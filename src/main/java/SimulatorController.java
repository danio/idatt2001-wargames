import Main.*;
import Unit.Unit;
import Unit.UnitFactory;

import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class SimulatorController {

    private Stage stage;

    // Middle
    @FXML
    private ListView<Event> battleLog;
    @FXML
    private ChoiceBox<Terrain> terrainChoice;

    // Left
    private SimpleStringProperty army1name;


    @FXML
    private Label army1Label;
    @FXML
    private Label army1Count;
    @FXML
    private Label army1Infantry;
    @FXML
    private Label army1Ranged;
    @FXML
    private Label army1Cavalry;
    @FXML
    private Label army1Commander;

    @FXML
    private TableView<Unit> army1Table;
    private ObservableList<Unit> army1Previous;
    @FXML
    private TextField army1UnitName;
    @FXML
    private ChoiceBox<String> army1UnitType;
    @FXML
    private Spinner<Integer> army1UnitHealth;
    @FXML
    private Spinner<Integer> army1UnitAttack;
    @FXML
    private Spinner<Integer> army1UnitArmor;
    @FXML
    private Button importButton1;
    @FXML
    private Button exportButton1;
    private SimpleStringProperty army2name;

    // Right
    @FXML
    private Label army2Label;
    @FXML
    private Label army2Count;
    @FXML
    private Label army2Infantry;
    @FXML
    private Label army2Ranged;
    @FXML
    private Label army2Cavalry;
    @FXML
    private Label army2Commander;


    @FXML
    private TableView<Unit> army2Table;
    private ObservableList<Unit> army2Previous;
    @FXML
    private TextField army2UnitName;
    @FXML
    private ChoiceBox<String> army2UnitType;
    @FXML
    private Spinner<Integer> army2UnitHealth;
    @FXML
    private Spinner<Integer> army2UnitAttack;
    @FXML
    private Spinner<Integer> army2UnitArmor;
    @FXML
    private Button importButton2;
    @FXML
    private Button exportButton2;

    /**
     * Set the stage object so that it's accessible for the button callbacks
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        // Left
        army1name = new SimpleStringProperty();
        army1name.set("Army 1");
        army1Label.textProperty().bindBidirectional(army1name);

        army1Table.getItems().addListener((InvalidationListener) l -> updateCounts1());

        initTable(army1Table);

        army1UnitType.getItems().addAll(UnitFactory.values());
        army1UnitType.setValue("InfantryUnit");
        Runnable updateDefaultStats1 = () -> {
            var defaultUnit = UnitFactory.getUnit(army1UnitType.getValue(), "tmp", army1UnitHealth.getValue());
            army1UnitAttack.getValueFactory().setValue(defaultUnit.getAttack());
            army1UnitArmor.getValueFactory().setValue(defaultUnit.getArmor());
        };
        army1UnitType.setOnAction(event -> updateDefaultStats1.run());

        army1UnitHealth.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 100));
        army1UnitAttack.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        army1UnitArmor.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));

        updateDefaultStats1.run();

        importButton1.setOnAction(event -> {
            try {
                importArmy(army1Table, army1name);
            } catch (IOException e) {
                error("Error importing army", e.getMessage());
            }
        });
        exportButton1.setOnAction(event -> {
            try {
                exportArmy(army1Table, army1name);
            } catch (IOException e) {
                error("Error exporting army", e.getMessage());
            }
        });

        // Right
        army2name = new SimpleStringProperty();
        army2name.set("Army 2");
        army2Label.textProperty().bindBidirectional(army2name);

        army2Table.getItems().addListener((InvalidationListener) l -> updateCounts2());

        initTable(army2Table);

        army2UnitType.getItems().addAll(UnitFactory.values());
        army2UnitType.setValue("InfantryUnit");
        Runnable updateDefaultStats2 = () -> {
            var defaultUnit = UnitFactory.getUnit(army2UnitType.getValue(), "tmp", army2UnitHealth.getValue());
            army2UnitAttack.getValueFactory().setValue(defaultUnit.getAttack());
            army2UnitArmor.getValueFactory().setValue(defaultUnit.getArmor());
        };
        army2UnitType.setOnAction(event -> updateDefaultStats2.run());

        army2UnitHealth.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 100));
        army2UnitAttack.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        army2UnitArmor.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));

        updateDefaultStats2.run();

        importButton2.setOnAction(event -> {
            try {
                importArmy(army2Table, army2name);
            } catch (IOException e) {
                error("Error importing army", e.getMessage());
            }
        });
        exportButton2.setOnAction(event -> {
            try {
                exportArmy(army2Table, army2name);
            } catch (IOException e) {
                error("Error exporting army", e.getMessage());
            }
        });

        // Middle
        battleLog.setItems(FXCollections.observableList(new ArrayList<>()));

        terrainChoice.getItems().setAll(Terrain.values());
        terrainChoice.setValue(Terrain.PLAINS);

        // Default
        army1Table.getItems().addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));
        army2Table.getItems().addAll(UnitFactory.getUnits("InfantryUnit", 5, "Meele Creep", 550));

        army1Previous = FXCollections.observableList(army1Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));
        army2Previous = FXCollections.observableList(army2Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));
    }

    @SuppressWarnings("unchecked")
    private void initTable(TableView<Unit> table) {
        TableColumn<Unit, String> typeCol = new TableColumn<>("Type");
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));

        TableColumn<Unit, String> nameCol = new TableColumn<>("Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Unit, String> HPCol = new TableColumn<>("Health");
        HPCol.setCellValueFactory(new PropertyValueFactory<>("health"));

        // Unchecked suppression is for this, this is safe since all the types are hopefully actually valid TableColumns
        // Generally this would be unsafe, but since it is static and not dynamically adding elements it should be fine
        // as long as developers don't add bad types to the list.
        table.getColumns().addAll(typeCol, nameCol, HPCol);

        table.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.DELETE) {
                var selected = table.getSelectionModel().getSelectedItem();
                table.getItems().remove(selected);
            }
        });
    }

    private void updateCounts1() {
        var army = new Army("tmp", army1Table.getItems());
        army1Count.setText("(" + army.getAllUnits().size() + ")");
        army1Infantry.setText("I: (" + army.getInfantryUnits().size() + ")");
        army1Ranged.setText("R: (" + army.getRangedUnits().size() + ")");
        army1Cavalry.setText("C: (" + army.getCavalryUnits().size() + ")");
        army1Commander.setText("G: (" + army.getCommanderUnits().size() + ")");
    }

    private void updateCounts2() {
        var army = new Army("tmp", army2Table.getItems());
        army2Count.setText("(" + army.getAllUnits().size() + ")");
        army2Infantry.setText("I: (" + army.getInfantryUnits().size() + ")");
        army2Ranged.setText("R: (" + army.getRangedUnits().size() + ")");
        army2Cavalry.setText("C: (" + army.getCavalryUnits().size() + ")");
        army2Commander.setText("G: (" + army.getCommanderUnits().size() + ")");
    }

    @FXML
    private void simulate() {
        battleLog.getItems().clear();

        army1Previous = FXCollections.observableList(army1Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));
        army2Previous = FXCollections.observableList(army2Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));

        var army1 = new Army("Army 1", army1Table.getItems());
        var army2 = new Army("Army 2", army2Table.getItems());

        var battle = new Battle(army1, army2, terrainChoice.getValue());

        battle.simulate(battleLog.getItems(), null);

        army1Table.refresh();
        army2Table.refresh();
    }

    @FXML
    private void step() {
        if (battleLog.getItems().size() == 0) {
            army1Previous = FXCollections.observableList(army1Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));
            army2Previous = FXCollections.observableList(army2Table.getItems().stream().map(Unit::clone).collect(Collectors.toList()));
        }

        var army1 = new Army("Army 1", army1Table.getItems());
        var army2 = new Army("Army 2", army2Table.getItems());

        var battle = new Battle(army1, army2, terrainChoice.getValue());

        battle.simulate(battleLog.getItems(), 1);

        army1Table.refresh();
        army2Table.refresh();
    }

    @FXML
    private void reset() {
        army1Table.setItems(army1Previous);
        army2Table.setItems(army2Previous);

        battleLog.getItems().clear();

        army1Table.getItems().addListener((InvalidationListener) l -> updateCounts1());
        army2Table.getItems().addListener((InvalidationListener) l -> updateCounts2());

        updateCounts1();
        updateCounts2();
    }

    @FXML
    private void addUnitArmy1() {
        var items = army1Table.getItems();
        var unit = UnitFactory.getUnit(
                army1UnitType.getValue(),
                army1UnitName.getText(),
                army1UnitHealth.getValue());
        items.add(unit);
    }

    @FXML
    private void addUnitArmy2() {
        var items = army2Table.getItems();
        var unit = UnitFactory.getUnit(
                army2UnitType.getValue(),
                army2UnitName.getText(),
                army2UnitHealth.getValue());
        items.add(unit);
    }

    private void importArmy(TableView<Unit> table, SimpleStringProperty name) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import csv file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Armies", "*.txt", "*.csv", "*.army"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        var selected = fileChooser.showOpenDialog(stage);
        if (selected != null) {
            var army = ArmyFileHandler.fromCsv(selected.toPath());
            table.getItems().clear();
            table.getItems().addAll(army.getAllUnits());
            name.set(army.getName());
        }
    }

    private void exportArmy(TableView<Unit> table, SimpleStringProperty name) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export to csv file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Armies", "*.txt", "*.csv", "*.army"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        fileChooser.setInitialFileName(name.getValue() + ".csv");
        var selected = fileChooser.showSaveDialog(stage);
        if (selected != null) {
            ArmyFileHandler.writeArmyFile(new Army(name.getValue(), table.getItems()), selected.toPath());
        }
    }

    private void error(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.setTitle(title);
        alert.showAndWait();
    }
}
