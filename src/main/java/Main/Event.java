package Main;
import Unit.Unit;

/**
 * Record for what happens in the battle
 */
public class Event {
    private final Unit subject;
    private final Unit object;
    private int damage;

    public Event(Unit subject, Unit object) {
        this.subject = subject;
        this.object = object;
    }

    public Unit getSubject() {
        return subject;
    }

    public Unit getObject() {
        return object;
    }

    public int getDamage() {
        return damage;
    }

    /**
     * @param damage how much damage the subject did to the object
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * @return english sentence describing the events of the battle
     */
    @Override
    public String toString() {
        return getSubject().getName() + "(" + getSubject().getHealth() + ")" + " attacked "
                + getObject().getName() + "(" + getObject().getHealth() + "->" + Math.max(getObject().getHealth() - getDamage(), 0) + ")" + " for "
                + getDamage() + " damage";
    }
}
