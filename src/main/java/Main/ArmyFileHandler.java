package Main;

import Unit.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Importing and exporting CSV files from armies.
 */
public class ArmyFileHandler {

    /**
     *
     * @param army army to convert
     * @return csv file in string format
     */
    public static String toCsv(Army army) {
        var mainBuilder = new StringBuilder();
        mainBuilder.append(army.getName()).append("\n");
        for (var unit : army.getAllUnits() ) {
            mainBuilder.append(String.format("%s,%s,%s,%s,%s\n",
                    unit.getClass().getSimpleName(),
                    unit.getName(),
                    unit.getHealth(),
                    unit.getAttack(),
                    unit.getArmor()
            ));
        }
        return mainBuilder.toString();
    }

    /**
     *
     * @param army army to convert to file
     * @param path path to file you want to save in
     * @throws IOException if something goes wrong
     */
    public static void writeArmyFile(Army army, Path path) throws IOException {
        var csv = toCsv(army);
        var writer = new BufferedWriter(new FileWriter(path.toFile()));
        writer.write(csv);
        writer.close();
    }

    /**
     * @param csv a comma separated string with all the files
     * @return army consisting of units from the list
     */
    public static Army fromCsv(String csv) {
        var text = csv.split("\n");
        var army = new Army(text[0]);
        var list = Collections.synchronizedList(new ArrayList<Unit>());
        Arrays.stream(text).skip(1).forEach(l -> {
            var cells = l.split(",");

            if (cells.length != 5) {
                throw new RuntimeException("Malformed csv file, should have 5 words, had:" + cells.length);
            }

            var type = cells[0];
            var name = cells[1];
            var health = cells[2];
            var attack = cells[3];
            var armor = cells[4];

            Unit unit = UnitFactory.getUnit(type, name, Integer.parseInt(health), Integer.parseInt(attack), Integer.parseInt(armor));

            list.add(unit);

        });

        army.addAll(list);
        return army;
    }

    /**
     * parse a csv file into an army
     * @param path file path to read csv from
     * @return
     * @throws IOException
     */
    public static Army fromCsv(Path path) throws IOException {
        var csv = Files.readString(path);
        return fromCsv(csv);
    }

}
