package Main;

import java.util.List;
import java.util.Random;

/**
 * Simulates battles between two armies
 */
public class Battle {
    private final Army armyOne;
    private final Army armyTwo;
    private final Terrain terrain;

    private final Random rng;

    /**
     * Construct a battle
     *
     * @param armyOne One of the armies to duke it out
     * @param armyTwo The other army to duke it out
     * @param rng     The random number generator to use for the duking
     * @throws NullPointerException if any of the parameters are null
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain, Random rng) {
        if (armyOne == null || armyTwo == null) {
            throw new NullPointerException("Armies cannot be null");
        }
        if (rng == null) {
            throw new NullPointerException("rng - random number generator, cannot be null");
        }
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
        this.rng = rng;
    }

    /**
     * Construct a battle
     *
     * @param armyOne One of the armies to duke it out
     * @param armyTwo The other army to duke it out
     * @throws NullPointerException if any of the armies are null
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        this(armyOne, armyTwo, terrain, new Random());
    }

    /**
     * Selects a random unit from the armies to attack a random unit from the opposing army
     * if the attacked unit dies, it is removed from the list
     *
     * @throws RuntimeException    if armies have no units
     * @throws ArithmeticException if the units attacking each other would lead to integer overflow or underflow
     */
    private void battle(List<Event> battlelog) {
        // Pick one of the armies to attack, the other is defense.
        Army attackers = rng.nextBoolean() ? armyOne : armyTwo;
        Army defenders = attackers.equals(armyOne) ? armyTwo : armyOne;

        var attacker = attackers.getRandom();
        var defender = defenders.getRandom();

        Event event = null;
        if (battlelog != null) {
            event = new Event(attacker.clone(), defender.clone());
        }

        attacker.attack(defender, this.terrain);

        if (battlelog != null) {
            event.setDamage(event.getObject().getHealth() - defender.getHealth());
            battlelog.add(event);
        }

        if (defender.getHealth() <= 0) {
            defenders.remove(defender);
        }
    }

    /**
     * battle with no log
     */
    private void battle() {
        battle(null);
    }

    /**
     * let the armies fight, continues until one of the armies are completely annihilated.
     *
     * @return the victorious army, now probably reduced in numbers
     * @throws RuntimeException    if armies have no units
     * @throws ArithmeticException if the units attacking each other lead to integer overflow or underflow
     */
    public Army simulate(List<Event> battlelog, Integer maxRounds) {
        int currentStep = 0;
        // (maxRounds != null -> currentStep < maxRounds ) && (armyOne.hasUnits() && armyTwo.hasUnits()) I miss my implication operator :(
        while ((maxRounds == null || currentStep < maxRounds) && (armyOne.hasUnits() && armyTwo.hasUnits())) {
            battle(battlelog);
            currentStep++;
        }
        @SuppressWarnings("UnnecessaryLocalVariable")
        var victor = armyOne.hasUnits() ? armyOne : armyTwo;
        return victor;
    }

    /**
     * simulate with no log, until one army is completely annihilated.
     * @return winning army
     */
    public Army simulate() {
        return simulate(null, null);
    }
}
