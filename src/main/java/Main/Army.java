package Main;
import Unit.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Main.Army stores a list of units and includes a method to fetch a random unit
 */
public class Army {
    private final String name;
    // This is a list because we use indexes to get a random unit, else this could've been the weaklier constrained Collection
    private final List<Unit> units;

    private final Random rng; // Wasn't specified in task but dependency injection will make it easier to test

    /**
     * Construct an army using dependency injection
     *
     * @param name  name of the army
     * @param units underlying list for units
     * @param rng   the random number generator to use for the methods in the army
     * @throws NullPointerException if anything is list is null
     */
    public Army(String name, List<Unit> units, Random rng) {
        if (name == null) {
            throw new NullPointerException("name cannot be null");
        }
        if (units == null) {
            throw new NullPointerException("units cannot be null");
        }
        if (rng == null) {
            throw new NullPointerException("rng cannot be null");
        }
        this.name = name;
        this.units = units;
        this.rng = rng;
    }

    /**
     * Construct an army, units are an arrayList, rng is a new Random object.
     *
     * @param name name of the army
     * @throws NullPointerException if name is null
     */
    public Army(String name) {
        this(name, new ArrayList<>(), new Random());
    }

    /**
     * Construct an army injecting the underlying list.
     *
     * @param name  name of the army
     * @param units underlying list to store the units of the army for dependency injection
     * @throws NullPointerException if name or units is null
     */
    public Army(String name, List<Unit> units) {
        this(name, units, new Random());
    }

    /**
     * Construct an army, injecting the random number generator
     *
     * @param name name of the unit
     * @param rng  random number generator to use for methods in army
     */
    public Army(String name, Random rng) {
        this(name, new ArrayList<>(), rng);
    }

    public String getName() {
        return name;
    }

    /**
     * Adds a unit to the army
     *
     * @param unit unit to add to the army
     * @throws NullPointerException          if the specified element is null
     * @throws UnsupportedOperationException if the add operation is not supported by the underlying list
     * @throws IllegalStateException         if the element cannot be added at this time due to insertion restrictions on the underlying list
     */
    public void add(Unit unit) {
        if (unit == null) {
            throw new NullPointerException("unit cannot be null");
        }
        units.add(unit);
    }

    /**
     * adds a collection of units to the underlying list of the army
     *
     * @param units a collection of units to add to the army
     * @throws NullPointerException          if the specified collection of units contains one or more null elements or if units is null
     * @throws UnsupportedOperationException if the addAll operation is not supported by the underlying list
     * @throws IllegalStateException         if not all the elements can be added at this time due to insertion restrictions on the underlying list
     */
    public void addAll(Collection<Unit> units) {
        if (units == null) {
            throw new NullPointerException("units cannot be null");
        }
        units.forEach(u -> {
            if (u == null) {
                throw new NullPointerException("List of units cannot contain null");
            }
        });
        this.units.addAll(units);
    }

    /**
     * Remove a unit from the army
     *
     * @param unit unit to remove
     * @throws NullPointerException if unit is null
     */
    public void remove(Unit unit) {
        if (unit == null) {
            throw new NullPointerException("You shouldn't try to remove null");
        }
        units.remove(unit);
    }

    /**
     * Check if army has units
     *
     * @return true if there are units in the army. false if not
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Get a list of all the units
     *
     * @return unmodifiable list of mutable units
     */
    public List<Unit> getAllUnits() {
        return List.copyOf(units);
    }

    /**
     * Get a random unit from the army
     *
     * @return random unit from the army
     * @throws RuntimeException if army contains no units
     * @see #hasUnits()
     */
    public Unit getRandom() {
        if (units.size() == 0) {
            throw new RuntimeException("There are no units in the army to select");
        }
        return units.get(rng.nextInt(units.size()));
    }

    /**
     * get all units matching the predicate
     *
     * @param p predicate for filtering
     * @return list of units matching given predicate
     */
    public List<Unit> getUnitsFilter(Predicate<Unit> p) {
        return units.stream().filter(p).collect(Collectors.toList());
    }

    /**
     * Gets all units matching the given class
     *
     * @param c   class extending Unit
     * @param <T> A Unit or anything that extends unit
     * @return A list of units of that class
     */
    public <T extends Unit> List<T> getUnitsOfClass(Class<T> c) {
        // This is fine, since getUnitsFilter() is returning Units which are T
        // We're just converting the objects back from being generalized to Unit
        @SuppressWarnings("unchecked")
        var result = (List<T>) getUnitsFilter(p -> p.getClass().equals(c));
        return result;
    }

    /**
     * @return List of infantry units
     */
    public List<InfantryUnit> getInfantryUnits() {
        return getUnitsOfClass(InfantryUnit.class);
    }

    /**
     * @return List of cavalry units
     */
    public List<CavalryUnit> getCavalryUnits() {
        return getUnitsOfClass(CavalryUnit.class);
    }

    /**
     * @return List of ranged units
     */
    public List<RangedUnit> getRangedUnits() {
        return getUnitsOfClass(RangedUnit.class);
    }

    /**
     * @return List of commander units
     */
    public List<CommanderUnit> getCommanderUnits() {
        return getUnitsOfClass(CommanderUnit.class);
    }

    /**
     * Checks whether armies are similar to each other
     * It does this by comparing whether all the units' string representations are the same
     * <p>
     * This is probably not a very good ide to use for anything important.
     *
     * @param other other army to compare
     * @return true if armies are similar
     */
    public boolean similar(Army other) {
        var self = this.getAllUnits().stream().map(u -> u.toString()).collect(Collectors.toList());
        var them = other.getAllUnits().stream().map(u -> u.toString()).collect(Collectors.toList());
        return self.equals(them);
    }

    /**
     * get the army as a string mostly for debug
     *
     * @return a multiline string describing the army
     */
    @Override
    public String toString() {
        var units = getAllUnits();

        var stringUnits = units.stream().map(unit -> unit.toString()).collect(Collectors.joining("\n\t"));
        return "The Main.Army of " + getName() + ":\n\t" + stringUnits + "\n";
    }
}
