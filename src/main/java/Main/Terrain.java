package Main;

/**
 * Types of terrains a battle will take place on.
 */
public enum Terrain {
    HILL,
    PLAINS,
    FOREST
}
