{ lib
, stdenv
, nix-gitignore
, jdk
, jre_minimal
, makeWrapper
, wrapGAppsHook
, glib
, gsettings-desktop-schemas
}:

let
  jre = jre_minimal.override {
    inherit jdk;
    modules = [
      "java.base"
      "javafx.base"
      "javafx.fxml"
      "javafx.controls"
    ];
  };
in
stdenv.mkDerivation rec {
  pname = "wargames";
  version = "0.0.0";

  src = nix-gitignore.gitignoreSource [] ./..;

  nativeBuildInputs = [ makeWrapper jdk wrapGAppsHook glib ];
  buildInputs = [ gsettings-desktop-schemas ];

  #dontWrapGApps = false;

  buildPhase = ''
    runHook preBuild

    javac -d target/classes \
      -classpath target/classes: \
      -sourcepath src/main/java: \
      -s target/generated-sources/annotations \
      -g -target 11 -source 11 \
      src/main/java/SimulatorApplication.java

    cp -r src/main/resources/* target/classes
    cd target/classes
    jar cvf wargames.jar *

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/wargames
    cp wargames.jar $out/share/wargames/

    mkdir -p $out/bin
    makeWrapper ${jre}/bin/java $out/bin/wargames \
      ''${gappsWrapperArgs[@]} \
      --add-flags "-cp $out/share/wargames/wargames.jar SimulatorApplication"

    runHook postInstall
  '';
}