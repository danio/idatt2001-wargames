{pkgs ? import <nixpkgs> { } }:
rec {
  package = pkgs.callPackage ./.nix/package.nix { jdk = pkgs.jdk11; };
}